---
title: My Journey to OSCP
date: 2021-07-02
---

## Why this blog?

I had a few people asking me questions like, how I prepared for the exam, what tools I used, the difficulty of the exam and some other questions. Here I will also be discussing a little bit about my workflow the resources I used and some of the scripts I made to make my life a little simple.

## Prerequisites for OSCP

- Technically, you must have a good networking knowledge. 
- You must also have good understanding of Bash scripting and experience with python (both python2 and python3). 
- You must have understanding of Linux and Windows System administration.
- Apart from that, you must have determination, as the exam can be a little bit gruelling if you don't think straight during the exam. It is the most important thing. You must be willing to make sacrifices. 


In short, you cannot just jump into this course without knowing anything or having zero experience in cyber security.
Personally, I had some prior experience in cyber security. I have participated in lots of CTFs, solved many HTB boxes and challenges and watched a huges number of videos from different Cybersec Youtubers.

**For more information on the Exam prerequisites please visit the official website : [Details about PWK Course](https://www.offensive-security.com/pwk-oscp/)**

## How I started of with my prepataion

First of all, I am not very good at penetration testing, neither am I very good at web application testing. I struggled a lot with web based vulnerabilities like SQL injection and XSS. My main interest is in reverse engineering and binary exploitation. Also, I had not solved HTB boxes in quite sometime, so I needed to get back into shape. For this I chose the obvious resource, TJ Null's list of retired HTB boxes. 

I decided that I would take one month to do a good number of boxes from the list before taking up lab. I wanted be comfortable with pentesting before taking up the lab. So, I started with the HTB boxes. I solved around 45 boxes during the month. Apart from solving boxes, I also watched Ippsec's videos on various boxes as you get to know about a lot of new techniques and how you can streamline your workflow while solving the boxes. Not only you get to learn new things from Ippsec videos, you also get into the mindset of performing a penetrationg testing engagement. This mindset is very important as you need it throughout the course as well as during the exam.

After solving HTB boxes for a month, watching Ippsec videos and taking a copious amount of notes, I felt that I was ready to take PWK labs.

## My Experience with the PWK Labs

I booked the PWK Labs for one month and got an email confirming the time and date when my lab time was going to start and when I would get the study materials. On the stipulated date I got my lab connection VPN and the study materials. Although I was a bit confused with the lab control panel, my friend (who had cleared OSCP a few months back), helped me figure it out. 

On the first day itself I had booked my exam. I got my suitable time for exam, around 2 weeks after my lab time was over. So, I would get a little bit more time to go through the study materials.

I started studying the materials and solving the lab boxes from day one itself. I would watch videos in the morning and after that I would start solving the boxes. I tried to solve at least 3 boxes each day. Sometimes I would meet my goal and sometimes I wouldn't and sometimes I would solved even 4-5 boxes a day. Gradually I unlocked one of the subnets. 

Continuing into the subnet I learned lots of things about pivoting. While I was solving the subnet, 5 of the OSCP examination machines were retired and put into that particular subnet. They proved to be an excellent way to understand the examination difficulty and to get you into the exam mindset. Then there was the added difficulty of using the pivot. Gradually throughout my lab time I completed around 53 machines in the lab and went through most of the study material. 

Since, I had around 2 weeks weeks before the exam, I went through rest of the study materials thoroughly and took more notes. I kept solving more HTB boxes, watching Ippsec videos and taking notes. I had pracitced the Buffer Overflow exercises in the PWK Lab environment. After my lab time was over, I kept practicing buffer overflow in the [Buffer Overflow Prep](https://tryhackme.com/room/bufferoverflowprep) room at TryHackMe. I solved all the exercises there and kept a note of the mistakes that I was making during the exercises.


## The Day Before the Exam

On the day before the exam, I prepared my Kali Linux VM for the exam. I had updated my notes, cloned the required repositories that would be required during the examination. For example, nishang reverse shells, privilege escalation awsome suite, the impacket repositories and some others. I went to bed early that day, as my exam was at 9:30 AM the next day and I needed to be properly rested.


## My Experience during the exam

At 9:00 AM I logged in to the proctoring app to complete my verification steps. After completing my verification I waited till 9:30 AM, when I got my exam VPN connection and details regarding my exam objectives.

So I started with the buffer overflow and finished it within 1 hour. Then I solved the 10 pointer box in 30 minutes. After that I moved on to one of the 20 points machine and got the user shell after a little bit of poking around. Then I got stuck in the privilege escalation part of that box. I wasted a lot of time there. After wasting around 3 hours and failing to get the privesc on that box, I moved on to the next 20 points box. 

Here I panicked and got stuck on the next box as well. I couldn't even get the initial shell on this box. I was really frustrated. So, I went and had dinner, took a little break and came back to the box. After further inspection of the box, I found out a very interesting way to get code execution. Through that code execution, I got reverse shell. After getting the initial shell, it took me very less time to get the privilege escalation. 

However, I still needed to privesc on the previous box to get passing marks. So, I reset the box to clear all the exploits that I was throwing at it. I went through my notes, and got my hints for the privesc. Now I had passing marks, and there was still some time, before my exam ended. I spent that time, taking copious screenshots of the solved boxes, taking note of the important commands that I had run, the exploits that I used, the references that helped me identify the vulnerabilities, etc. At around 4 AM I decided that I had enough screenshots, so I slept for around 1.5 hours. I asked the proctor to temporarily switch off the webcam, as I needed to rest.

At 5:30AM I woked up and started going through my notes, taking note of the changes that I made. I made sure that I get full marks for the boxes that I had solved. 

At 9:15 AM my exam was over and my VPN was disconnected. I got proper sleep after that and started working on my report. The next morning I submitted my report. 

A few days after submitting my report, I got the email confirming that I had passed the OSCP certification exam and I got my digital badge. 


### Things to remember during the exam

- Take small breaks every hour. **DO NOT** be like me. I took breaks every 3-4 hours, which almost broke my body
- Try to get proper sleep. **DO NOT** sleep 1.5 hours like me. 
- Note down every important commands and tools that you use during the exam. 
- Note down references and scripts during the exam
- Take proper screenshots. Refer to the PEN-200 [Exam Guide](https://help.offensive-security.com/hc/en-us/articles/360040165632)
- Eat light food, so that you don't feel sleepy.
- Drink lots of water. 
- When you feel morally down, talk to your friends and family. 
- Rejoice each and every point you score, it will keep you morally boosted.

### Things to remember while writing the report 

- I would suggest you use the exam templates provided by Offensive Security. For this again, refer to [Exam Guide](https://help.offensive-security.com/hc/en-us/articles/360040165632)

- Mention all the important commands and scripts that you have used.
- Give reference of all the exploits that you have used. 
- Be precise and professional with your report.
- Rest should be clear from the Exam Guide
- I used the [LibreOffice Template](https://www.offensive-security.com/pwk-online/PWKv1-REPORT.odt), since I don't have Microsoft Word.


## Resources and Scripts that I used for Exam Preparation and during the lab

- Before taking the PWK I used [TJ Null's](https://docs.google.com/spreadsheets/d/1dwSMIAPIam0PuRBkCiDI88pU3yzrqqHkDtBngUHNCw8/edit#gid=1839402159) list as a reference to solve the retired boxes. 
- [IppSec](https://www.youtube.com/channel/UCa6eh7gCkpPo5XXUDfygQQA) videos also helped me a lot to learn new things
- Apart from the PWK Labs I used [TryHackMe Buffer Overflow Prep](https://tryhackme.com/room/bufferoverflowprep) to keep practise for the Buffer Overflow.

These are the resources that I used before taking up the PWK Course.

### Tools and scripts I used throughout my journey

- The most important tools used was [Obsidian](https://obsidian.md/) to take notes. 
- For privilege escalation I used : [privilege-escalation-awesome-scripts-suite](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite)
- For Reverse shells I used [Pentest Monkey Reverse Shell Cheat Sheet](https://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet)
- For Windows Reverse Shells I used : [Nishang Reverse shell](https://github.com/samratashok/nishang)
- To filter out just the port numbers from the `.gnmap` file I created a ripoff script [Filter Ports Script](https://github.com/dosxuz/Filter-Ports)
- To create workspaces for all the targets, I created a script : [Workspace Creator](https://github.com/dosxuz/Workspace_creator) (**NOTE: The workspaces are accorsding to my own needs. Make changes to the script to customize it to your own needs**)
- I used [Impacket tools](https://github.com/SecureAuthCorp/impacket)
- Windows binaries for impacket [Impacket Example Windows](https://github.com/maaaaz/impacket-examples-windows)
- Useful static binaries [Important Static binaries](https://github.com/andrew-d/static-binaries)

### My Workflow that I used for the journey

- I used tmux to manage my workflow
- I created a separate tmux session for each box.

Here is the link to my [Tmux config file](https://github.com/dosxuz/useful_stuff/blob/master/tmux.conf) if you're interested. It is partly taken from Ippsec's tmux config, and partly from internet. It uses vim bindings mostly.


## My Final Thoughts on the PWK Course and the OSCP Exam

The PWK is an excellent course if you want to have a strong understanding of VAPT. They have also included the Active Directory part in their syllabus, which I was particularly weak at. The course does an excellent job at helping you understand several concepts in-depth. It wastes no time in demonstrating the techniques and concepts. 

The OSCP is a very practical which tests your understanding of the course hands-on. Looking back at the exam, it seems that the boxes were really simple, but as I said, I did panic a little bit. 

As a final thought, the PWK Course was really worth it. 

If you're taking the course, be prepared to grind hard for the labs. Some of the lab machines are gruelling but very rewarding. Unlike HTB boxes they are very practical and similar to the real world.

