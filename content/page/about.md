---
title: About me
subtitle: whoami
comments: false
---

Hi. This is Ritaban Das. I am a Cyber Security Researcher, Red Teamer, Malware Developer, Malware Analyst, Reverse Engineer, Exploit Developer, Programmer and pianist. 

Follow my blog to know more about reverse engineering, exploit development, malware development and much more.
